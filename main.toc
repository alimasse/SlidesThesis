\beamer@endinputifotherversion {3.36pt}
\select@language {english}
\beamer@sectionintoc {1}{Introduction}{2}{0}{1}
\beamer@sectionintoc {2}{De Bruijn graph}{6}{0}{2}
\beamer@sectionintoc {3}{Scaling}{10}{0}{3}
\beamer@sectionintoc {4}{Read mapping}{19}{0}{4}
\beamer@sectionintoc {5}{Improving assembly }{28}{0}{5}
\beamer@sectionintoc {6}{Conclusion}{40}{0}{6}
